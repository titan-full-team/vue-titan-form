import TitanForm from './form/TitanForm.vue'
import {
    faAngleLeft,
    faAngleRight,
    faCheck,
    faCheckCircle,
    faTimes,
    faTimesCircle,
    faMinus,
    faPlus,
    faGripLines,
    faTrashAlt,
    faCloudUploadAlt
} from "@fortawesome/free-solid-svg-icons";

const formElementsIcons = [
    faAngleLeft,
    faAngleRight,
    faTimes,
    faCheck,
    faCheckCircle,
    faTimesCircle,
    faMinus,
    faPlus,
    faGripLines,
    faTrashAlt,
    faCloudUploadAlt
];

export default {
    install(Vue) {
        Vue.component('TitanForm', TitanForm);
    }
}

export {TitanForm, formElementsIcons};
export const setDeepValue = (obj, access, value) => {
    if (typeof (access) == 'string') {
        access = access.split('.');
    }
    if (access.length > 1) {
        if (obj[access[0]] === undefined) {
            obj[access[0]] = {};
        }
        setDeepValue(obj[access.shift()], access, value);
    } else {
        obj[access[0]] = value;
    }
}
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import {FontAwesomeIcon} from "@fortawesome/vue-fontawesome";
import {library} from '@fortawesome/fontawesome-svg-core'
import {formElementsIcons} from "@/plugin";

Vue.use(Vuex);
Vue.component("font-awesome-icon", FontAwesomeIcon);

Vue.config.productionTip = false;

formElementsIcons.forEach((icon) => {
    library.add(icon)
});

new Vue({
    render: h => h(App),
}).$mount('#app');

export default {
    props: {
        i18n: {
            type: Function,
            required: false,
            default: (slug) => {
                return slug;
            }
        }
    }
}
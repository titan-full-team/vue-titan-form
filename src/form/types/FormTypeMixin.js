import FormMixin from "@/form/FormMixin";

export const formType = {
    mixins: [FormMixin],
    props: {
        model: {
            type: Object,
            required: true,
        },
        schema: {
            required: true,
        },
        options: {
            required: true
        }
    }
}
# Installation
```
npm install --save git+ssh://git@gitlab.com:titan-full-team/vue-titan-form.git
```
Note: Install font awesome and include all icons which you wanna use!

## Titan buttons
### How to use
```
todo
```
OR 
```
todo
```


### Props
| Name | Type | Default | Required | Note |
|------|------|---------|----------|------|
| key     | string |        | true         | Button identifier     |
| type  | string |         | true          | Button type: `button`, `action`, `link`
| name     | string |        | true         | Button name     |
| icon     | string or array |        | false         | Font awesome icon (must by included manually in project)     |
| styleClass  | string | `btn-primary`        | false          | Button style class     |
| visible  | boolean or function | true        | false          | Is button visible     |
| to  | string or function |         | true          | URL (Type link only)     |
| target  | string | `_self`        | false          | Html link target (_self, _blank etc.. Type link only)     |
| modalText  | string |         | true          | Text inside modal window (Type action only)     |
| modalIcon  | string or array | `icon` parameter        | false          | Icon inside modal window (Type action only)     |
### Events
| Name | params | Note |
|------|------|---------|
| `@click` | button key parameter | Event is triggered when user clicks to button or accept action button   |
| `@click-{button-key}` | all button parameters | Same as `click` but for specific button only
